import React from 'react'
import { Button, Card } from 'react-bootstrap'

function CardProduct({children}) {
  return (
    <>
       <Card style={{ width: '18rem', margin: '0 10px' }}>
        <Card.Img variant="top" />
        <Card.Body>
          <Card.Title>{children.title}</Card.Title>
          <Card.Text>
                {children.description.slice(0,150)}...
          </Card.Text>
          <Button variant="primary">Modifier</Button>
        </Card.Body>
      </Card>
    </>
  )
}

export default CardProduct
