import React from 'react'
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import { Link } from 'react-router-dom';
function Navigator() {
    return (
        <div>
            <Navbar expand="lg" className="bg-body-tertiary">
                <Container>
                    <Navbar.Brand >Gestion Produits</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Nav className="me-auto " style={{ display: 'flex', gap: '30px' }}>
                        <Link to="/">Home</Link>
                        <Link to="/products">Products</Link>
                        <Link to="/add_product">Add</Link>

                    </Nav>
                </Container>
            </Navbar>
        </div>
    )
}

export default Navigator
