import React from 'react'
import useFetchProducts from '../customHooks/useFetchProducts'
import CardProduct from './CardProduct';
const homeStyle = {
  display: 'flex',
  flexWrap: 'wrap',
  gap:'40px',
  marginTop : 25
}
function Home() {
    const products = useFetchProducts();
  return (
    <div style={homeStyle}  >
      {
           products.map((product)=>{
            return <CardProduct>{product}</CardProduct>
        })
      }
    </div>
  )
}

export default Home
