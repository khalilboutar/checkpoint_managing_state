import React from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Home from './components/Home';
import Navigator from './components/Navigator';
import ContextProducts from './contexts/ContextProducts';
import AddProduit from './products/AddProduit';

function App() {
  return (
    <Router>
      <div>
        <ContextProducts>
          <Navigator />
          <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/products" element={<Home />} />
            <Route path="/add_product" element={<AddProduit />} />

          </Routes>
        </ContextProducts>

      </div>
    </Router>
  );
}

export default App;
